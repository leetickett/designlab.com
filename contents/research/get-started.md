---
name: Get started
---

As an open source project, we are very happy to accept community contributions to UX research.

The [UX Research handbook](https://about.gitlab.com/handbook/engineering/ux/ux-research) will give you a better understanding of how we conduct research at GitLab as well as access to UX research resources. Visit the [UX Research Insights repository](https://gitlab.com/gitlab-org/uxr_insights) to get an understanding of how we share research insights. If you don't already have an idea for a UX research study, you can browse through our [existing research](https://about.gitlab.com/handbook/engineering/ux/ux-research/#how-to-find-existing-research) as a starting point for inspiration.

Helpful Links:
* [UX Research FAQ](https://gitlab.com/gitlab-org/uxr_insights/#faqs)
* [UX Department Handbook](https://about.gitlab.com/handbook/engineering/ux/)
* [Contributing to UX Design](https://about.gitlab.com/community/contribute/ux-design/)


