<!--
Start by choosing one of the following based on the nature of your contribution:
   - For changes in the Pajamas UI Kit duplicate the file to your drafts: https://www.figma.com/community/file/781156790581391771
   - To create a new component, duplicate our component template to your drafts: https://www.figma.com/file/OmvFfWkqEsdGhXAND133ou/Component?node-id=0%3A1
   - For all other changes, create a new file in your drafts.

To move a duplicate file to your drafts:
   1. Open the duplicate, then use the dropdown next to the file name to select
      “Move to Project…” and select your Drafts as the new location.
   2. Update the template name and start designing ;)
-->

### Description

<!-- Add a short description of the changes you're contributing. Consider adding
a checklist of variations and states to the description so that reviewers can be
sure to cross reference everything that has been completed. -->

### Figma file

<!-- Before pasting the link to your Figma file/frame, in the file sharing settings,
make sure that “anyone with the link” can view. -->

[View Figma file →](ADD LINK TO FIGMA FILE/FRAME)

### Checklist

Make sure the following are completed before closing the issue:

1. [ ] **Assignee**: Design in your Figma draft file, following our [structure][structure],
   [building][building], and [annotation][annotation] guidelines. If you have any
   questions, reach out to a [FE/UX Foundations designer][foundations-team].
1. [ ] **Assignee**: Update the link to the Figma file in the issue description.
1. [ ] **Assignee**: Ask a [FE/UX Foundations designer][foundations-team]
   to review your changes (ensure they have edit permissions in Figma).
1. [ ] **Reviewer**: Review and approve assignee’s changes. Ensure that they include
   all variations/states and, if applicable, matches existing Sketch specs and
   is responsive.
1. [ ] **Reviewer**: Assign to a [Figma maintainer](https://about.gitlab.com/handbook/engineering/projects/#design.gitlab.com)
   for final review (make sure they have edit permissions in Figma).
1. [ ] **Maintainer**: Review and approve assignee’s changes.
1. [ ] **Maintainer**: Apply the changes to the **Pajamas UI Kit** file, and view
   the components in the Assets panel to ensure they align with our [asset library structure guidelines][structure].
1. [ ] **Maintainer**: [Publish][publishing] the library changes along with a clear
   commit message.
1. [ ] **Assignee**: Move your draft file to the **Component archive** Figma project.
   If you're a community contributor, we ask that you [transfer ownership of your draft file](https://help.figma.com/hc/en-us/articles/360040530853)
   to the maintainer so they can move it to our archive, along with its version
   history and comments.
1. [ ] **Assignee** (or Maintainer, for community contributions): If it's a new
   pattern or a significant change, add an agenda item to the next UX weekly call
   to inform everyone.
1. [ ] **Assignee**: Create a merge request in this repository with the [component-guideline template](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/blob/master/.gitlab/merge_request_templates/component-guideline.md)
   to create or update the component's documentation page. Link it here as a related
   merge request. Use `View design in Pajamas UI Kit →` for the link text. This
   replaces any existing link to Sketch Measure specs. Anyone with the link should
   be able to view the file. 
1. [ ] **Assignee**: [Create an issue in GitLab UI](https://gitlab.com/gitlab-org/gitlab-ui/-/issues/new)
   to build or update the component code. Mark the new issue as related to this one.
1. [ ] **Assignee**: 🎉 Congrats, you made it! You can now close this issue.

/label ~"UX" ~"Figma"

[annotation]: https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/blob/master/doc/pajamas-ui-kit.md#adding-descriptions-notes-and-annotations
[building]: https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/blob/master/doc/pajamas-ui-kit.md#building-components
[foundations-team]: https://about.gitlab.com/company/team/?department=fe-ux-foundations-team
[publishing]: https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/blob/master/doc/pajamas-ui-kit.md#publishing-changes
[structure]: https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/blob/master/doc/pajamas-ui-kit.md#structure
